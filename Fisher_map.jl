using LinearAlgebra

# Implementation of the Fisher information map for measurements.

function vectorization(C)
    V = deepcopy(C);
    a = V[:,1];
    b = length(V[1, :])
    for i in 2:b
        append!(a, V[:,i]);
    end;
    return(a);
end;


function Fisher_map(k);
    num_measurements = length(k);
    d = size(k[1])[1];
    U = Array{Matrix{Complex{Float64}}}(undef, num_measurements);
    for i in 1:num_measurements
        U[i] = 1/tr(k[i]) * vectorization(k[i]) * adjoint(vectorization(k[i]));
    end;

    sum = zeros(Complex{Float64}, d * d, d * d);
    for i in 1:num_measurements
         sum += U[i];
    end;
    return sum
end
