using Convex
using SCS
using LinearAlgebra

#= 
Determining the compatibility of two POVMs with arbitrary many outcomes.
If the reseult is >= 1, they are compatible.
=#


function compatibility(U::Array{Array{Matrix{ComplexF64}}},d::Integer)
    s = Variable(1);
    #num_POVMs = length(U);
    sum_1 = zeros(d, d);
    one = Matrix(I, d, d) * 1.0;
    G = [[ComplexVariable(d, d) for i=1:length(U[1])] for j=1:length(U[2])];
    constraints = [G[i][j] in :SDP for i = 1:length(U[1]) for j = 1:length(U[2])];

    for i in 1:length(U[1])
        for j in 1:length(U[2])
            sum_1 += G[i][j]
        end
    end
    constraints += sum_1 == one;
    # 1. POVM
    for i=1:length(U[1])
        sum_2 = zeros(d, d) * 1.0;
        for j=1:length(U[2])
            sum_2 += G[i][j]
        end
        constraints += sum_2 == (s * U[1][i] + (1 - s) * one * 1 / d);
    end
    # 2. POVM
    for k=1:length(U[2])
        sum_3 = zeros(d, d) * 1.0;
        for l=1:length(U[1])
            sum_3 += G[l][k]
        end
        constraints += sum_3 == (s * U[2][k] + (1 - s) * one * 1 / d);
    end

    problem =  maximize(s, constraints);
    solve!(problem, SCS.Optimizer; silent_solver = false);
    res = problem.optval;
    return res;
end
