using Convex
using SCS
using LinearAlgebra

#= 
Determining the compatibility of three POVMs with arbitrary many outcomes.
If the reseult is >= 1, they are compatible.
=#


function compatibility_3(U::Array{Array{Matrix{ComplexF64}}}, d::Integer)
    t = Variable(1);
    one = Matrix(I, d, d) * 1.0;
    sum_0 = zeros(d, d);
    G = [[[ComplexVariable(d, d) for k=1:length(U[1])] for j=1:length(U[2])] for i=1:length(U[3])];
    constraints = [G[k][j][i] in :SDP for k=1:length(U[1]) for j=1:length(U[2]) for i=1:length(U[3])];

    for k=1:length(U[1])
        for j=1:length(U[2])
            for i=1:length(U[3])
                sum_0 += G[k][j][i];
            end
        end
    end
    constraints += sum_0 == one;

    # 1. POVM
    for k = 1:length(U[1])
        sum_1 = zeros(d, d);
        for j=1:length(U[2])
            for i=1:length(U[3])
                sum_1 += G[k][j][i];
            end
        end
        constraints += sum_1 == (t * U[1][k] + (1 - t) * one * 1/d);
    end

    # 2. POVM
    for l = 1:length(U[2])
        sum_2 = zeros(d, d);
        for m=1:length(U[1])
            for n=1:length(U[3])
                sum_2 += G[m][l][n];
            end
        end
        constraints += sum_2 == (t * U[2][l] + (1 - t) * one * 1/d);
    end

    # 3. POVM
    for o = 1:length(U[3])
        sum_3 = zeros(d, d);
        for p=1:length(U[1])
            for q=1:length(U[2])
                sum_3 += G[p][q][o];
            end
        end
        constraints += sum_3 == (t * U[3][o] + (1 - t) * one * 1/d);
    end

    problem = maximize(t, constraints);
    solve!(problem, SCS.Optimizer; silent_solver = false);
    res = problem.optval;
    return res;
end
