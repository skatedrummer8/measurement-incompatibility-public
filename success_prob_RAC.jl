using StatsBase

# Determine the success probability of a RAC when using maximum likelihood encoding.
# n is the length of the dit string and d the dimension.

function p_succ_RAC(n::Integer,d::Integer)
    sum = 0;
    for i=0:d^n - 1
        b = digits(i, base = d, pad = n);
        a = collect(countmap(b));
        max = 0;
        for j=1:size(a)[1]
            if  a[j][2] > max;
                max = a[j][2];
            end
        end
        sum += max;
    end
    sum *= 1/(n * d^n);
    return sum;
end
