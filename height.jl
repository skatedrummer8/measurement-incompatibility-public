using Convex
using SCS
using LinearAlgebra

# Find the height of a given Matrix M.
# This is necessary for Zhu's criterion.


function h(array::Vector{Matrix{Complex{Float64}}})
    dim = size(array[1])[1];
    H = HermitianSemidefinite(dim, dim);
    n = size(array, 1);
    constr = [H - array[i] in :SDP for i in 1:n]
    problem = minimize(real(tr(H)), constr);

    solve!(problem, SCS.Optimizer; silent_solver = false);
    res = problem.optval;
    erg = H.value;
    return res, erg;
end