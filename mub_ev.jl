using LinearAlgebra

# Determine the eigenvectors of a mutual unbiased basis of odd dimension d.

# Define function, that creates unit vectors

unit_vec(i,n) = [j == i for j in 1:n];

# function calculating the t'th eigenvector of the k'th MUB

function mub_ev(d, t, k)
    omega = exp(2*pi*(0+1im)/d);
    ev = zeros(d) * (1.0 + 0.0im);
    for i in 0:d-1
        e = unit_vec(i+1,d) * (1.0 + 0.0im);
        ev += omega^(-i*t) * omega^(k *i*(i-1)/2) * e;
    end
    ev *= 1/sqrt(d)
    return(ev)
end
