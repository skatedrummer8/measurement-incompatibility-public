using Convex
using SCS
using LinearAlgebra

include("height.jl")
include("Fisher_map.jl")

#=
Detect incompatibility with Zhu's criterion.
It is assumed. that all measurements are quadratic and of the same dimension.
If the result is > dimension, the measurements are incompatible.
=#

#= 
Calling Fisher map : Fisher_map()
           height : h()
=#

function Zhu(array::Array{Array{Matrix{Complex{Float64}}}})
	num_POVMs = size(array)[1];
	X = Array{Matrix{Complex{Float64}}}(undef, num_POVMs);
	for i in 1:num_POVMs
		X[i] = Fisher_map(array[i]);
	end
	height = h(X);

	return height
end
