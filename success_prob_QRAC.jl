using LinearAlgebra
using Convex
using SCS
using Random

#=
Calculate the success probability of a Quantum Random Access Code, where
the encoding is optimized and the measurements are given.
=#

function p_succ(m::Vector{Vector{Matrix{ComplexF64}}})
    sum = Variable(1);
    n = size(m)[1];
    d = size(m[1])[1];
    A = [ComplexVariable(d, d) for j=1:d^n];
    constraints = [A[j] in :SDP for j=1:d^n];
    for j=1:d^n
        constraints += real(tr(A[j])) == 1.0
    end
    sum2 = zeros(d,d);
    for i=0:d^n -1
        b = digits(i, base = d, pad = n);
        p = reverse(b);
        for j in 1:n
            sum2 += A[i+1] *  m[j][p[j] + 1];
        end
    end
 
    sum2 *= 1 / (n * d^n);
    sum2 = tr(sum2);
    constraints += sum2 == sum;
    problem = maximize(real(sum), constraints);
    solve!(problem, SCS.Optimizer; silent_solver = false);
    res = problem.optval;
    return res;
end;
